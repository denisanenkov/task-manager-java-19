package ru.anenkov.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.anenkov.tm.api.repository.ICommandRepository;
import ru.anenkov.tm.api.repository.IProjectRepository;
import ru.anenkov.tm.api.repository.ITaskRepository;
import ru.anenkov.tm.api.repository.IUserRepository;
import ru.anenkov.tm.api.service.*;
import ru.anenkov.tm.command.*;
import ru.anenkov.tm.command.auth.*;
import ru.anenkov.tm.command.data.base64.DataBase64ClearCommand;
import ru.anenkov.tm.command.data.base64.DataBase64LoadCommand;
import ru.anenkov.tm.command.data.base64.DataBase64SaveCommand;
import ru.anenkov.tm.command.data.binary.DataBinaryClearCommand;
import ru.anenkov.tm.command.data.binary.DataBinaryLoadCommand;
import ru.anenkov.tm.command.data.binary.DataBinarySaveCommand;
import ru.anenkov.tm.command.data.json.DataJsonClearCommand;
import ru.anenkov.tm.command.data.json.DataJsonLoadCommand;
import ru.anenkov.tm.command.data.json.DataJsonSaveCommand;
import ru.anenkov.tm.command.data.xml.DataXmlClearCommand;
import ru.anenkov.tm.command.data.xml.DataXmlLoadCommand;
import ru.anenkov.tm.command.data.xml.DataXmlSaveCommand;
import ru.anenkov.tm.command.project.*;
import ru.anenkov.tm.command.project.ProjectClearCommand;
import ru.anenkov.tm.command.project.ProjectRemoveByIdCommand;
import ru.anenkov.tm.command.project.ProjectRemoveByIndexCommand;
import ru.anenkov.tm.command.project.ProjectRemoveByNameCommand;
import ru.anenkov.tm.command.project.ProjectShowCommand;
import ru.anenkov.tm.command.project.ProjectShowByIdCommand;
import ru.anenkov.tm.command.project.ProjectShowByIndexCommand;
import ru.anenkov.tm.command.project.ProjectShowByNameCommand;
import ru.anenkov.tm.command.project.ProjectUpdateByIdCommand;
import ru.anenkov.tm.command.project.ProjectUpdateByIndexCommand;
import ru.anenkov.tm.command.system.*;
import ru.anenkov.tm.command.task.*;
import ru.anenkov.tm.command.task.TaskRemoveByIdCommand;
import ru.anenkov.tm.command.task.TaskRemoveByIndexCommand;
import ru.anenkov.tm.command.task.TaskRemoveByNameCommand;
import ru.anenkov.tm.command.task.TaskClearCommand;
import ru.anenkov.tm.command.task.TaskShowByIdCommand;
import ru.anenkov.tm.command.task.TaskShowByIndexCommand;
import ru.anenkov.tm.command.task.TaskShowByNameCommand;
import ru.anenkov.tm.command.task.TaskShowCommand;
import ru.anenkov.tm.command.task.TaskUpdateByIdCommand;
import ru.anenkov.tm.command.task.TaskUpdateByIndexCommand;
import ru.anenkov.tm.command.user.*;
import ru.anenkov.tm.constant.MessageConst;
import ru.anenkov.tm.exception.system.IncorrectCommandException;
import ru.anenkov.tm.repository.CommandRepository;
import ru.anenkov.tm.repository.ProjectRepository;
import ru.anenkov.tm.repository.TaskRepository;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.service.*;
import ru.anenkov.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(taskService, userService, projectService);

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @SneakyThrows
    private void init() throws Exception {
        @NotNull final Reflections reflections = new Reflections("ru.anenkov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.anenkov.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz: classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if(isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
    }

    private void initUsers() {
        userService.create("1", "1", "test@mail.ru");
        userService.create("test", "test", "test@mail.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    @SneakyThrows
    private void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new IncorrectCommandException(String.valueOf(command));
        @Nullable final Role[] roles = command.roles();
        authService.checkRole(command.roles());
        command.execute();
    }

    public void run(@Nullable final String[] args) throws Exception {
        init();
        System.out.println(MessageConst.WELCOME);
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private static void logError(Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        return true;
    }

    @NotNull public IUserService getUserService() {
        return userService;
    }

    @NotNull public IAuthService getAuthService() {
        return authService;
    }

    @NotNull public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

}