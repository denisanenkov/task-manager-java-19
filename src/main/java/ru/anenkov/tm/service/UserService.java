package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.IUserRepository;
import ru.anenkov.tm.api.service.IUserService;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.empty.*;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.HashUtil;

import java.util.List;
import java.util.Objects;

public class UserService implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable String email) {
        return null;
    }

    @NotNull
    @Override
    public User removeUser(@Nullable final User user) {
        if (user == null) return null;
        return Objects.requireNonNull(userRepository.removeUser(user));
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return Objects.requireNonNull(userRepository.removeById(id));
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USER);
        return userRepository.add(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setRole(Role.USER);
        user.setEmail(email);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) return null;
        return userRepository.removeByEmail(email);
    }

    @Nullable
    @Override
    public User updateUserFirstName(
            @Nullable final String userId,
            @Nullable final String newFirstName
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (newFirstName == null || newFirstName.isEmpty()) return null;
        @Nullable User user = findById(userId);
        assert user != null;
        user.setFirstName(newFirstName);
        return user;
    }

    @Nullable
    @Override
    public User updateUserMiddleName(
            @Nullable final String userId,
            @Nullable final String newMiddleName
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (newMiddleName == null || newMiddleName.isEmpty()) return null;
        @Nullable User user = findById(userId);
        assert user != null;
        user.setMiddleName(newMiddleName);
        return user;
    }

    @Nullable
    @Override
    public User updateUserLastName(
            @Nullable final String userId,
            @Nullable final String newLastName
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (newLastName == null || newLastName.isEmpty()) return null;
        @Nullable User user = findById(userId);
        assert user != null;
        user.setLastName(newLastName);
        return user;
    }

    @Nullable
    @Override
    public User updateUserEmail(
            @Nullable final String userId,
            @Nullable final String newEmail
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (newEmail == null || newEmail.isEmpty()) return null;
        @Nullable User user = findById(userId);
        assert user != null;
        user.setEmail(newEmail);
        return user;
    }

    @Nullable
    @Override
    public User updatePassword(
            @Nullable final String userId,
            @Nullable final String newPassword
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (newPassword == null || newPassword.isEmpty()) return null;
        @Nullable final User user = findById(userId);
        assert user != null;
        user.setPasswordHash(HashUtil.salt(newPassword));
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(true);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User UnlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return user;
    }

    @Nullable
    @Override
    public User DeleteUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        userRepository.removeUser(user);
        return user;
    }

    @Override
    public void load(@Nullable final List<User> users) {
        if (users == null) return;
        userRepository.load(users);
    }

    @Override
    public void load(@Nullable final User... users) {
        if (users == null) return;
        userRepository.load(users);
    }

    @Nullable
    @Override
    public List<User> getUserList() {
        return userRepository.getListUsers();
    }

}
