package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.service.IAuthService;
import ru.anenkov.tm.api.service.IUserService;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.empty.EmptyLoginException;
import ru.anenkov.tm.exception.empty.EmptyPasswordException;
import ru.anenkov.tm.exception.user.AccessDeniedException;
import ru.anenkov.tm.util.HashUtil;

import javax.validation.constraints.Null;

public class AuthService implements IAuthService {

    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    @SneakyThrows
    public void checkRole(@Nullable final Role[] roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final String userId = getUserId();
        @Nullable final User user = userService.findById(userId);
        @Nullable final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (@Nullable final Role item : roles) {
            if (role.equals(item)) {
                return;
            }
            throw new AccessDeniedException();
        }
    }

    @Nullable
    @SneakyThrows
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    @SneakyThrows
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.isLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (email == null || email.isEmpty()) return;
        userService.create(login, password, email);
    }

    @Override
    @SneakyThrows
    public void updatePassword(@NotNull final String newPassword) {
        if (!isAuth()) throw new AccessDeniedException();
        userService.updateUserFirstName(userId, newPassword);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User showUserProfile() {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findById(userId);
    }

    @Override
    @SneakyThrows
    public void updateUserFirstName(@Nullable final String newFirstName) {
        if (!isAuth()) throw new AccessDeniedException();
        if (newFirstName == null || newFirstName.isEmpty()) return;
        userService.updateUserFirstName(userId, newFirstName);
    }

    @Override
    @SneakyThrows
    public void updateUserMiddleName(@Nullable final String newMiddleName) {
        if (!isAuth()) throw new AccessDeniedException();
        if (newMiddleName == null || newMiddleName.isEmpty()) return;
        userService.updateUserMiddleName(userId, newMiddleName);
    }

    @Override
    @SneakyThrows
    public void updateUserLastName(@Nullable final String newLastName) {
        if (!isAuth()) throw new AccessDeniedException();
        if (newLastName == null || newLastName.isEmpty()) return;
        userService.updateUserLastName(userId, newLastName);
    }

    @Override
    @SneakyThrows
    public void updateUserEmail(@Nullable final String newEmail) {
        if (!isAuth()) throw new AccessDeniedException();
        if (newEmail == null || newEmail.isEmpty()) return;
        userService.updateUserFirstName(userId, newEmail);
    }

}
