package ru.anenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.ICommandRepository;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.command.auth.*;
import ru.anenkov.tm.command.data.base64.DataBase64ClearCommand;
import ru.anenkov.tm.command.data.base64.DataBase64LoadCommand;
import ru.anenkov.tm.command.data.base64.DataBase64SaveCommand;
import ru.anenkov.tm.command.data.binary.DataBinaryClearCommand;
import ru.anenkov.tm.command.data.binary.DataBinaryLoadCommand;
import ru.anenkov.tm.command.data.binary.DataBinarySaveCommand;
import ru.anenkov.tm.command.data.json.DataJsonClearCommand;
import ru.anenkov.tm.command.data.json.DataJsonLoadCommand;
import ru.anenkov.tm.command.data.json.DataJsonSaveCommand;
import ru.anenkov.tm.command.data.xml.DataXmlClearCommand;
import ru.anenkov.tm.command.data.xml.DataXmlLoadCommand;
import ru.anenkov.tm.command.data.xml.DataXmlSaveCommand;
import ru.anenkov.tm.command.project.ProjectCreateCommand;
import ru.anenkov.tm.command.project.ProjectClearCommand;
import ru.anenkov.tm.command.project.ProjectRemoveByIdCommand;
import ru.anenkov.tm.command.project.ProjectRemoveByIndexCommand;
import ru.anenkov.tm.command.project.ProjectRemoveByNameCommand;
import ru.anenkov.tm.command.project.ProjectShowCommand;
import ru.anenkov.tm.command.project.ProjectShowByIdCommand;
import ru.anenkov.tm.command.project.ProjectShowByIndexCommand;
import ru.anenkov.tm.command.project.ProjectShowByNameCommand;
import ru.anenkov.tm.command.project.ProjectUpdateByIdCommand;
import ru.anenkov.tm.command.project.ProjectUpdateByIndexCommand;
import ru.anenkov.tm.command.system.*;
import ru.anenkov.tm.command.task.TaskCreateCommand;
import ru.anenkov.tm.command.task.TaskRemoveByIdCommand;
import ru.anenkov.tm.command.task.TaskRemoveByIndexCommand;
import ru.anenkov.tm.command.task.TaskRemoveByNameCommand;
import ru.anenkov.tm.command.task.TaskClearCommand;
import ru.anenkov.tm.command.task.TaskShowByIdCommand;
import ru.anenkov.tm.command.task.TaskShowByIndexCommand;
import ru.anenkov.tm.command.task.TaskShowByNameCommand;
import ru.anenkov.tm.command.task.TaskShowCommand;
import ru.anenkov.tm.command.task.TaskUpdateByIdCommand;
import ru.anenkov.tm.command.task.TaskUpdateByIndexCommand;
import ru.anenkov.tm.command.user.*;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        commandList.add(new HelpCommand());
        commandList.add(new SystemInfoCommand());
        commandList.add(new LoginCommand());
        commandList.add(new LogoutCommand());
        commandList.add(new UserDeleteCommand());
        commandList.add(new RegisterCommand());
        commandList.add(new UserProfileViewCommand());
        commandList.add(new UserPasswordUpdateCommand());
        commandList.add(new UserUpdateEmailCommand());
        commandList.add(new UserUpdateFirstNameCommand());
        commandList.add(new UserUpdateMiddleNameCommand());
        commandList.add(new UserUpdateLastNameCommand());
        commandList.add(new VersionCommand());
        commandList.add(new AboutCommand());
        commandList.add(new ArgumentsViewCommand());
        commandList.add(new CommandsViewCommand());
        commandList.add(new TaskShowCommand());
        commandList.add(new TaskClearCommand());
        commandList.add(new TaskCreateCommand());
        commandList.add(new TaskShowByIndexCommand());
        commandList.add(new TaskShowByIdCommand());
        commandList.add(new TaskShowByNameCommand());
        commandList.add(new TaskUpdateByIndexCommand());
        commandList.add(new TaskUpdateByIdCommand());
        commandList.add(new TaskRemoveByIdCommand());
        commandList.add(new TaskRemoveByIndexCommand());
        commandList.add(new TaskRemoveByNameCommand());
        commandList.add(new ProjectShowCommand());
        commandList.add(new ProjectClearCommand());
        commandList.add(new ProjectCreateCommand());
        commandList.add(new ProjectShowByIndexCommand());
        commandList.add(new ProjectShowByIdCommand());
        commandList.add(new ProjectShowByNameCommand());
        commandList.add(new ProjectUpdateByIndexCommand());
        commandList.add(new ProjectUpdateByIdCommand());
        commandList.add(new ProjectRemoveByIdCommand());
        commandList.add(new ProjectRemoveByIndexCommand());
        commandList.add(new ProjectRemoveByNameCommand());
        commandList.add(new DataBinarySaveCommand());
        commandList.add(new DataBinaryLoadCommand());
        commandList.add(new DataBinaryClearCommand());
        commandList.add(new DataBase64ClearCommand());
        commandList.add(new DataBase64LoadCommand());
        commandList.add(new DataBase64SaveCommand());
        commandList.add(new DataJsonSaveCommand());
        commandList.add(new DataJsonLoadCommand());
        commandList.add(new DataJsonClearCommand());
        commandList.add(new DataXmlClearCommand());
        commandList.add(new DataXmlSaveCommand());
        commandList.add(new DataXmlLoadCommand());
        commandList.add(new ExitCommand());
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommands() {
        return commandList;
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}