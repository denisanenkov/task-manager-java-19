package ru.anenkov.tm.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Project extends AbstractEntity {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Nullable
    private String userId = "";

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}