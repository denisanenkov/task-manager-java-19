package ru.anenkov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.exception.system.IncorrectCommandException;
import ru.anenkov.tm.repository.CommandRepository;

import java.util.List;

public interface SearchCommandUtil {

    static void commandFind(@Nullable String arg) {
        boolean isFinded = false;
        @NotNull final CommandRepository commandRepository = new CommandRepository();
        @Nullable final List<AbstractCommand> commands = commandRepository.getCommands();
        for (@NotNull AbstractCommand command : commands) {
            if (command.name().equals(arg)) isFinded = true;
        }
        if (!isFinded) throw new IncorrectCommandException(arg);
    }

}