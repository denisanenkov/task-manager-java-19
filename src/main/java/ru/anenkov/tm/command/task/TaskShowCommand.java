package ru.anenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.enumeration.Role;

import java.util.List;

public class TaskShowCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @Nullable final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        for (@Nullable Task task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
