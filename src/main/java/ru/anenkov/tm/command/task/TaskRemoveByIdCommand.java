package ru.anenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Task-remove-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete task from task - list by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        @Nullable final Task task = serviceLocator.getTaskService().removeOneById(userId, id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
