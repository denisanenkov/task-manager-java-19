package ru.anenkov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class RegisterCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Registry";
    }

    @NotNull
    @Override
    public String description() {
        return "Registry new user";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL: ");
        @NotNull final String email = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
