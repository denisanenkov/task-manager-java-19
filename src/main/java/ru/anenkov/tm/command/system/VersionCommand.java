package ru.anenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.enumeration.Role;

public class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "Version";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version info.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.1.4");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
