package ru.anenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.service.ServiceLocator;
import ru.anenkov.tm.bootstrap.Bootstrap;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.enumeration.Role;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class ArgumentsViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public final String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String name() {
        return "Arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @Nullable final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (@Nullable AbstractCommand command : commands) {
            if (command.arg() != null) {
                System.out.println(command.arg());
            }
        }
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
