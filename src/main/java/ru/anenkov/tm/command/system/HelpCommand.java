package ru.anenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.bootstrap.Bootstrap;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.enumeration.Role;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "Help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[Help]");
        @NotNull final Bootstrap bootstrap = (Bootstrap) serviceLocator;
        @Nullable final Collection<AbstractCommand> commands = bootstrap.getCommands();
        for (@Nullable AbstractCommand command : commands) {
            System.out.println(command.name() + " - " + command.description());
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
