package ru.anenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class ProjectRemoveByNameCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Project-remove-by-name";
    }

    @Override
    public String description() {
        return "Delete project from project - list by name";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @Nullable final Project project = serviceLocator.getProjectService().removeOneByName(userId, name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
