package ru.anenkov.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.dto.Domain;
import ru.anenkov.tm.enumeration.Role;

import javax.xml.crypto.Data;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataJsonLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Data-json-load";
    }

    @Override
    public String description() {
        return "Load data from json file";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA JSON LOAD]");
        @Nullable final String path = DataConst.FILE_JSON;
        if (path == null) return;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(path)));
        @NotNull Domain domain = objectMapper.readValue(json, Domain.class);
        serviceLocator.getDomainService().load(domain);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
