package ru.anenkov.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public class DataXmlClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Data-xml-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear data from xml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML CLEAR]");
        @NotNull final File file = new File(DataConst.FILE_XML);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}