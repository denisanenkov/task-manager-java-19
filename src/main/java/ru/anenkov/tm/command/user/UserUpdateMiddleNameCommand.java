package ru.anenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class UserUpdateMiddleNameCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Update-middle-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Update middle name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.println("ENTER NEW USER MIDDLE NAME: ");
        @NotNull final String newMiddleName = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updateUserMiddleName(newMiddleName);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
