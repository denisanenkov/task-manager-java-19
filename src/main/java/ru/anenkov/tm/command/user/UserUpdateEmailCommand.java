package ru.anenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class UserUpdateEmailCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Update-email";
    }

    @NotNull
    @Override
    public String description() {
        return "Update email";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER MAIL]");
        System.out.println("ENTER NEW USER MAIL: ");
        @NotNull final String newUserEmail = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updateUserEmail(newUserEmail);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
