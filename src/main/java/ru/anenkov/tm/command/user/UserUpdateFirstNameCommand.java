package ru.anenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

import javax.validation.constraints.Null;

public class UserUpdateFirstNameCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Update-first-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Update first name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER FIRST NAME]");
        System.out.println("ENTER NEW USER FIRST NAME: ");
        @NotNull final String newFirstName = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updateUserFirstName(newFirstName);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
