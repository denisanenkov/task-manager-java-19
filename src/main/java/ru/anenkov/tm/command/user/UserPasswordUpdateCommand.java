package ru.anenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class UserPasswordUpdateCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Update-user-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Update user password";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD: ");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updatePassword(newPassword);
        System.out.println("[PASSWORD UPDATED]");
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }
}
