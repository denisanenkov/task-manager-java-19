package ru.anenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

public interface ICommandRepository {

    @NotNull
    List<AbstractCommand> commandList = new ArrayList<>();

    @NotNull
    Class[] COMMANDS = {};

    @NotNull
    List<AbstractCommand> getCommands();

    @NotNull
    List<AbstractCommand> getCommandList();

}
