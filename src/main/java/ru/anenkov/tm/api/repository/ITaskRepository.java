package ru.anenkov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Task;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

public interface ITaskRepository {

    @NotNull
    void add(@NotNull String userId, @NotNull Task task);

    @NotNull
    void remove(@NotNull String userId, @NotNull Task task);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    @NotNull
    void clear(@NotNull String userId);

    @NotNull
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task removeOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    void clear();

    @NotNull
    void merge(@NotNull Collection<Task> tasks);

    @NotNull
    Task merge(@NotNull Task task);

    @NotNull
    void merge(@NotNull Task... tasks);

    @NotNull
    void load(@NotNull Collection<Task> tasks);

    @NotNull
    void load(@NotNull Task... tasks);

    @NotNull
    void load(@NotNull Task task);

    @Nullable
    List<Task> getListTasks();

}
