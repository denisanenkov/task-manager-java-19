package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;

import javax.validation.constraints.Null;
import java.util.List;

public interface IUserService {

    @Nullable
    List<User> findAll();

    @Nullable
    User create(String login, String password);

    @Nullable
    User create(String login, String password, String email);

    @Nullable
    User create(String login, String password, Role role);

    @Nullable
    User findById(String id);

    @Nullable
    User findByLogin(String login);

    @Nullable
    User findByEmail(String email);

    @NotNull
    User removeUser(User user);

    @NotNull
    User removeById(String id);

    @NotNull
    User removeByLogin(String login);

    @NotNull
    User removeByEmail(String email);

    @Nullable
    User updateUserFirstName(String userId, String newFirstName);

    @Nullable
    User updateUserMiddleName(String userId, String newMiddleName);

    @Nullable
    User updateUserLastName(String userId, String newLastName);

    @Nullable
    User updateUserEmail(String userId, String newEmail);

    @Nullable
    User updatePassword(String userId, String newPassword);

    @Nullable
    User lockUserByLogin(String login);

    @Nullable
    User UnlockUserByLogin(String login);

    @Nullable
    User DeleteUserByLogin(String login);

    void load(List<User> users);

    void load(User... users);

    @Nullable
    List<User> getUserList();

}
