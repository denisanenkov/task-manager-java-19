package ru.anenkov.tm.constant;

public interface DataConst {

    String FILE_BINARY = "./data.bin";

    String FILE_BASE64 = "./database64.base64";

    String FILE_XML = "./data.xml";

    String FILE_JSON = "./data.json";

}
